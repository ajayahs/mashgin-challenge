Download the caffe model from mashgin-fcn-alexnet/caffemodel-url <br />
Create folders log/ and snapshot/train/ <br />

Ensure Caffe is installed in the system and Python Layer is enabled. <br />
(Python Layer can be enabled by compiling caffe with option WITH_PYTHON_LAYER=1) <br />

To run training: <br />
caffe train --solver mashgin-fcn-alexnet/solver.prototxt --weights mashgin-fcn-alexnet/fcn-alexnet-pascal.caffemodel --logdir log/ <br />

Note: You might have to add the current directory to PYTHONPATH. Do this by executing following command on the shell: <br />
export PYTHONPATH="$PWD:$PYTHONPATH" <br />
This needs to be done so that mashgin_layers.py module is visible to caffe. <br />

To modify solver and train prototxt files:
- Edit the Architecture in net.py
- Run python net.py -- This will create new solver and train prototxt files

Next steps:
- Data augmentation : Random crops, flips, split the image into multiple parts (4 quadrants + center)
- Try VGG16, Resnet etc. models
- Try Skip connections to encode context information and reduce the upsampling error (like FCN8s, FCN16s and FCN32s)